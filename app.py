
from flask import Flask, jsonify, request, abort
from flask.helpers import make_response
from flask.wrappers import Response
from sqlalchemy import create_engine, select, and_, text, String
from sqlalchemy.dialects import postgresql
import pandas as pd
import json


app = Flask(__name__)
dbengine = None

def conditional_pair(keypair, rp):
    # ensures both values in pair are present
    return (keypair[0] in rp) and (keypair[1] in rp)

def conditional_eitheror(key1, keypair, rp):
    # ensures either key1 or keypair are present.
    return (key1 in rp) or conditional_pair(keypair, rp)

def validate_presence(rp):
    if not conditional_pair(('latitude', 'longitude'), rp):
        abort(make_response(jsonify({'message': 'latitude and longitude must be present'}), 400))
    if not conditional_eitheror('price', ('minprice', 'maxprice'), rp):
        abort(make_response(jsonify({'message': 'Either price, or pair of (minprice, maxprice) must be present'}), 400))
    if not conditional_eitheror('bedrooms', ('minbedrooms', 'maxbedrooms'), rp):
        abort(make_response(jsonify({'message': 'Either bedrooms, or pair of (minbedrooms, maxbedrooms) must be present'}), 400))
    if not conditional_eitheror('bathrooms', ('minbathrooms', 'maxbathrooms'), rp):
        abort(make_response(jsonify({'message': 'Either bathrooms, or pair of (minbathrooms, maxbathrooms) must be present'}), 400))

def validate_search_params(rp):
    validate_presence(rp)
    # print(int(rp.get('minbedrooms') or 0))
    try:
        round(rp['latitude'] or 1, 6)
        round(rp['longitude'] or 1, 6)
    except:
        abort(make_response(jsonify({'message': 'latitude,longitude must be an valid real value'}), 400))
    try:
        int(rp.get('price') or 1) and int(rp.get('minprice') or 1) and int(rp.get('maxprice') or 1)
    except:
        abort(make_response(jsonify({'message': 'price must be an number'}), 400))
    try:
        
        int(rp.get('bedrooms') or 1) and int(rp.get('minbedrooms') or 1) and int(rp.get('maxbedrooms') or 1)
    except:
        abort(make_response(jsonify({'message': 'bedrooms must be an number'}), 400))
    try:
        int(rp.get('bathrooms') or 1) and int(rp.get('minbathrooms') or 1) and int(rp.get('maxbathrooms') or 1)
    except:
        abort(make_response(jsonify({'message': 'bathrooms must be an number'}), 400))

def init_db(host, port, dbname, username, password):
    try:
        engine = create_engine(f'postgresql://{username}:{password}@{host}:{port}/{dbname}')
        print("Initiated database connection", engine)
    except Exception as ex:
        print("Got an exception connecting to database")
        raise ex
    # print(engine.execute('select * from property limit 10').fetchall())
    return engine

def dbsearch_property(rp, offset=10, limit=25):
    latitude, longitude = rp['latitude'], rp['longitude']
    price, minprice, maxprice = None, None, None
    bathrooms, minbathrooms, maxbathrooms = None, None, None
    bedrooms, minbedrooms, maxbedrooms = None, None, None
    location_query = text(
        """
        SELECT id, st_distance(geog, ST_POINT(:longitude, :latitude))*0.000621371192 as dist, 
        latitude, longitude, geog, bedrooms, bathrooms
        from property
        where st_distance(geog, ST_POINT(:longitude, :latitude)) < 16093.44 --16093.44 is 10 miles
        order by geog <-> ST_POINT(:longitude, :latitude)
        limit 200 offset 0
        """
    )
    location_weight_clause = text(
        """
        CASE WHEN (dist<2) then 0.3 --full score
        WHEN (dist>10) then 0 -- no score
        ELSE  (1- (dist-2)/(10-2))*0.3 
        END
        AS distance_weight
        """
    )

    if 'price' in rp:
        price = rp['price']
        price_query = text(
            """
            SELECT  id, price
            FROM property
            WHERE price < :price*1.5
            """
        )
        price_weight_clause = text(
            """
            CASE WHEN (1.0*price/ :price < 1.10) then 0.3 -- we don't mind showing lower prices
            WHEN (1.0*price/ :price > 1.5) then 0 -- limit to 150pct of ask price
            ELSE (1 - (1.0*price/ :price - 1.1)/(1.5-1.1))*0.3
            END
            AS price_weight
            """
    )
    else:
        minprice, maxprice = rp['minprice'], rp['maxprice'] 
        price_query = text(
            """
            SELECT id, price
            FROM property
            WHERE price between :minprice AND :maxprice
            """
        )
        price_weight_clause = text("0.3 AS price_weight")
    if 'bedrooms' in rp:
        bedrooms = rp['bedrooms']
        bedrooms_weight_clause = text(
            """
            CASE WHEN (bedrooms<1) then 0
            WHEN (bedrooms=:bedrooms) then 0.2
            WHEN (bedrooms>:bedrooms) then 0.10 
            WHEN (bedrooms<:bedrooms) then 0.10
            END
            AS bedrooms_weight
            """
        )
    else:
        minbedrooms, maxbedrooms = rp['minbedrooms'], rp['maxbedrooms']
        bedrooms_weight_clause = text(
            """
            CASE WHEN (bedrooms<1) then 0
            WHEN (bedrooms BETWEEN :minbedrooms AND :maxbedrooms) then 0.2
            WHEN (bedrooms<:minbedrooms) then 0
            WHEN (bedrooms>:maxbedrooms) then 0.1 -- do not mind having more bedrooms
            END
            AS bedrooms_weight
            """
        )
    if 'bathrooms' in rp:
        bathrooms = rp['bathrooms']
        bathrooms_weight_clause = text(
            """
            CASE WHEN (bathrooms<1) then 0
            WHEN (bathrooms=:bathrooms) then 0.2
            WHEN (bathrooms>:bathrooms) then 0.10 
            WHEN (bathrooms<:bathrooms) then 0.10
            END
            AS bathrooms_weight
            """
        )
    else:
        minbathrooms, maxbathrooms = rp['minbathrooms'], rp['maxbathrooms']
        bathrooms_weight_clause = text(
            """
            CASE WHEN (bathrooms<1) then 0
            WHEN (bathrooms BETWEEN :minbathrooms AND :maxbathrooms) then 0.2
            WHEN (bathrooms<:minbathrooms) then 0
            WHEN (bathrooms>:maxbathrooms) then 0.1 -- do not mind having more bathrooms
            END
            AS bathrooms_weight
            """
        )
    
    select_clause = text(
        """
        SELECT id, price, bedrooms, bathrooms, latitude, longitude, dist as distance,
        distance_weight+price_weight+bedrooms_weight+bathrooms_weight as total_weight
        """
    )
    inner_select_clause = text(
        """
        SELECT distance_table.id, distance_table.dist, price_table.price,
        distance_table.bedrooms, distance_table.bathrooms, 
        distance_table.latitude, distance_table.longitude
        """\
        +\
        "," + str(location_weight_clause)\
        +\
        "," + str(price_weight_clause)\
        +\
        "," + str(bedrooms_weight_clause)\
        +\
        "," + str(bathrooms_weight_clause)
    )

    order_by = " ORDER BY total_weight DESC LIMIT :limit OFFSET :offset "
    query = str(select_clause) + " FROM ( " + str(inner_select_clause) + " FROM (" + str(location_query) + ") AS distance_table " +\
        " INNER JOIN (" + str(price_query) + " ) AS price_table " +\
            " on distance_table.id=price_table.id "+\
                " ) as result_table " + order_by
    
    print(query)
    resultproxy = dbengine.execute(text(query),
        latitude=latitude,
        longitude=longitude,
        price=price,
        minprice=minprice,
        maxprice=maxprice,
        bathrooms=bathrooms,
        minbathrooms=minbathrooms,
        maxbathrooms=maxbathrooms,
        bedrooms=bedrooms,
        minbedrooms=minbedrooms,
        maxbedrooms=maxbedrooms,
        limit=limit,
        offset=offset
    )
    resultset = resultproxy.fetchall()
    if(len(resultset) > 0):
        df = pd.DataFrame(resultset)
        df.columns = resultset[0].keys()
        print(df)
    else:
        df = pd.DataFrame(['No property found for search criteria'],  columns=['error'])
    return df.to_json(index=False, orient='table')


@app.route('/', methods=['GET'])
def hello_world():
    return jsonify({'message':'Hello, World!'})

@app.route('/api/v1/property/search', methods=['POST'])
def search_property():
    keys = [
        'latitude', 'longitude',
        'price', 'minprice', 'maxprice',
        'bedrooms', 'minbedrooms', 'maxbedrooms',
        'bathrooms', 'minbathrooms', 'maxbathrooms'
    ]
    # for pagination
    offset = request.args.get('start') or 0
    limit = request.args.get('limit') or 25
    print(offset, limit)
    if not request.json:
        abort(400)
    rp = request.json #request_params
    validate_search_params(rp)
    result = dbsearch_property(rp, offset, limit)
    modresult = json.loads(result)
    del modresult['schema']
    return (modresult)
    # print(json.loads(result)['schema'])
    return result
    # return jsonify({'keys': request.json.keys()})
    # return request.json

@app.errorhandler(400)
def error400(error):
    return make_response(jsonify({'error': 'Request body may not be correct'}), 400)

@app.errorhandler(404)
def not_found(error):
    print("eror", error)
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    dbengine = init_db(host='localhost', port=5432, dbname='radiusagent', username='postgres', password='protected')
    app.run(debug=True)
