# Property Locator
Searches for nearest housing properties around the given location, with search criteria such as Proximity, Budget, Number of bedrooms, Number of bathrooms, etc.

## Installation
### clone the Project
You can clone the project and run once all dependencies are satisfied. It is recommended to run via virtual environment [Instructions here](https://virtualenv.pypa.io/en/latest/installation/).

```bash
git clone https://sumitj39@bitbucket.org/sumitj39/property-locator.git
cd property-locator
# optional, install python virtual environment, and activate
pip install -r requirements.txt
```

## Data ingestion
### by running below script, new file will be created which includes random sample data for ingesting into database
```bash
# create data with 10000 rows
python data_ingestion.py -n 10000
```

## Database creation.
Database is created in a docker container.  
You need to run a few steps before you can use the database.  
Since we already have CSV data, using PostgreSQL's COPY command, we can copy bulk data into database in no time!
```bash
docker build -f postgresql/Dockerfile  --tag "radiusdb" .
# docker image 'radiusdb' is built. Now bring up the container # expose 5432 to outside network
docker run -d -p 5432:5432 radiusdb
```

## Consuming the API.
First run the flask application
```bash
python app.py
```

Go to any REST client such as PostMan and send a post request to http://127.0.0.1:5000/api/v1/property/search?start=0&limit=25   
with body as:
```
{
	"latitude": 40.706236,
	"longitude": -73.88932,
	"price": 450000, 
	"minbedrooms": "-2",
	"maxbedrooms": 4,
	"minbathrooms": 2,
	"maxbathrooms": 4
}
```
And response will be:
```
{
  "data": [
    {
      "bathrooms": 2,
      "bedrooms": 3,
      "distance": 1.5238797197,
      "id": 22120,
      "latitude": 40.699696,
      "longitude": -73.8616,
      "price": 355141,
      "total_weight": 1.0
    },
    {
      "bathrooms": 2,
      "bedrooms": 1,
      "distance": 1.6433894112,
      "id": 21173,
      "latitude": 40.688602,
      "longitude": -73.868286,
      "price": 492041,
      "total_weight": 1.0
    },
    ....
    ....
    {
      "bathrooms": 1,
      "bedrooms": 3,
      "distance": 2.557618044,
      "id": 23838,
      "latitude": 40.687504,
      "longitude": -73.93134,
      "price": 650546,
      "total_weight": 0.51984599
    }
}
```
Lot of options are available:  
```
1. latitude, longitude are mandatory
2. Either price or pair of (minprice, maxprice) is mandatory
3. Either bedrooms or pair of (minbedrooms, maxbedrooms) is mandatory
4. Either bathrooms or pair of (minbathrooms, maxbathrooms) is mandatory
```

## Improvements
1. When dealing with millions of records, and searches, we can cluster properties based on geography so that we narrow down the choices
2. Hold in memory cached database for faster response(such as memcached)  
3. We can have multiple specialized services, each dedicated to its own task -   
For eg: A Locator-Service that only gives nearest properties to given location,  
and another Price-service that only gives properties with budget constraints.  
And finally a request facing service that delegates requests to these specific dedicated services.
4. A UI for showing locations on map
## Screenshots
### Sample response
![Alt text](https://user-images.githubusercontent.com/13235252/71380323-b78de880-25f4-11ea-8994-4602855cdc2c.png)
### Distribution of sample locations(in red) around a point(blue)
![Alt text](https://user-images.githubusercontent.com/13235252/71380355-d8563e00-25f4-11ea-9701-ba73a3fa02e0.png)
## Exploring data and its characteristics, and some other experiments
The file inject-data.ipynb Jupyter Notebook contains interesting experiments and results.  
Make sure you take a look.
## Sources
1. Geo-data https://public.opendatasoft.com/explore/dataset/1000-largest-us-cities-by-population-with-geographic-coordinates/export/?sort=-rank
## Assumptions
1. Validation - the price, bedrooms, bathrooms are always positive( on negative values the rest api would return no data)  
2. For budget, when only one price is mentioned, The upper cap is 150%, i.e., upto 50% more than ask price are shown  
3. The radius around which data are generated is kept as 30KM, although this can be changed in data_ingest.py file.

