#!/usr/bin/env python
# coding: utf-8

# # Data ingestion
# This notebook is used as a experimental playground for creating real-world emulating scenarios.
# Some of them include
# #### 1. Real coordinates of top 10 US cities by population
# #### 2. Traffic generation according to population weightage (i.e. more traffic is generated from more populated cities)
# #### 3. Housing market price emulation - The prices of housing market is assumed to follow Log-normal distribution.
# #### this assumption is applied to generate random prices of houses across US.

# ## Import necessary libraries

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math
from haversine import haversine
from collections import Counter
import numpy as np
import click

plt.rcParams["figure.figsize"] = (12,9)


def create_random_loc(latitude, longitude, radius, size=1):
    #x0-longitude, y0-latitude
    # latitude, longitude: in degrees
    # radius - in meters
    # https://gis.stackexchange.com/a/25883
    # https://gis.stackexchange.com/a/168969
    
    # 1. convert radius in meters to radius in degrees
    # 1 degree = 111300 meters of earth surface at Equator
    '''
        var r = 100/111300 // = 100 meters
      , y0 = original_lat
      , x0 = original_lng
      , u = Math.random()
      , v = Math.random()
      , w = r * Math.sqrt(u)
      , t = 2 * Math.PI * v
      , x = w * Math.cos(t)
      , y1 = w * Math.sin(t)
      , x1 = x / Math.cos(y0)

    newY = y0 + y1
    newX = x0 + x1
    '''
    rDegree = radius/111300
    # 2. Adjust for shrinking of earth surface when we move north or south
    y0 = latitude
    x0 = longitude
    u = np.random.uniform(size=size)
    v = np.random.uniform(size=size)
    w = rDegree*np.sqrt(u)
    t = 2*np.pi*v
    x1 = w * np.cos(t)
    y1 = w* np.sin(t)
    x1 = x1 /np.cos(np.radians(latitude)) # Caution!- What happens when cos(π)? 
    return ( np.round(y1+y0, 6), np.round(x1+x0, 6))


# Create a distribution of location across geography (US only in this case)
# 20kms of radius. A good assumption
def create_sample_population(coordinates, size=50000, p=None, radius=20000):
    # coordinates - list of tuple(latitude, longitude) for cities (n_clusters)
    choices = np.random.choice(len(coordinates), size=size, p=p)
    counters = Counter(choices)
    sample_population = []
    for key in sorted(counters.keys()):
        lat, long = create_random_loc(latitude=coordinates[key][0], longitude=coordinates[key][1],
                                      radius=radius, size=counters[key])
        sample_population.append((lat, long))
    return sample_population



def create_cities():
    pd.read_csv('./1000-largest-us-cities-by-population-with-geographic-coordinates.csv', 
    sep=";", index_col='Rank').sort_index().head(10)[[ 'City', 'Population', 'Coordinates']].to_csv('top10cities.csv') # take the 1000 largest cities, delete unwanted columns, take only 10 rows and store in top10cities file

    print(f"Stored top 10 cities in top10cities.csv file")
    df = pd.read_csv('./top10cities.csv', index_col='Rank')
    sm = df['Population'].sum()
    df['Weightage'] = df['Population']/sm
    df['Weightage'].values # this weightage will be used to add users to corresponding cities.
    return df


@click.command()
@click.option('-n', default=10000, help='Number of rows to generate and insert in database')
def main(n):
    print(f"creating {n} rows with generated sample data")
    df = create_cities()
    # create coordinates in this format: [(lat1, lng1), (lat2, lng2), (lat3, lng3)]
    coordinates = list(map(lambda x: list(float(xx) for xx in x.split(',')), df['Coordinates'].values))

    sample_population = create_sample_population(coordinates, size=n, p=df['Weightage'].values)
    bedrooms = np.random.choice(np.arange(1, 5), size=n, p=[0.2, 0.3, 0.3, 0.2]) # assign bedrooms with probabilities
    bathrooms = np.random.choice(np.arange(1, 4), size=n, p=[0.3, 0.5, 0.2]) # assign bathroom probs.
    prices = np.random.lognormal(np.log(400000), 0.3, size=n).astype(int) #create a log-normal distribution of prices

    locations = pd.DataFrame()
    lats, longs = np.array([]), np.array([])
    for sp in sample_population:
        lat, long = sp
        lats = np.append(lats, lat)
        longs = np.append(longs, long)
    locations['latitude'] = lats
    locations['longitude'] = longs
    locations['bedrooms'] = bedrooms
    locations['bathrooms'] = bathrooms
    locations['prices'] = prices

    locations.to_csv('properties.csv', index=False)
    print(f"successfully stored {n} rows in properties.csv file")


if __name__ == "__main__":
    main()
