-- create the mail table property that holds all house information
create table property (
        id serial primary key,
        latitude real,
        longitude real,
        price integer,
        bedrooms integer,
        bathrooms integer,
        geog geography
);

-- copy the property.csv file to table
copy property(latitude, longitude, bedrooms, bathrooms, price) from '/properties.csv' with CSV DELIMITER ',' HEADER;

-- update the geography 
update property set geog=st_point(longitude, latitude);

-- create the geography index to fasten performance
CREATE INDEX  property_geog_idx
    ON property
    using GIST(geog);